<?php
/* @var $this yii\web\View */
$this->title = 'Productos';
Yii::$app->view->params['title-seo'] = 'Productos';
//Yii::$app->view->params['description-seo'] 
?>



<section class="productos">
    <h1>Productos</h1>
    <form action="?" method="get">
        <label for="q" >Search</label>

        <input type="search" name="q" id="q" value="<?= $search ?>" placeholder="Buscar" class="search__field">

        <button type="submit" class="search__btn">
           Buscar
        </button>
    </form> 

    <?= $this->render('_list_productos', [ 'dataProvider' => $productos_dataprovider ]); ?>
</section>




