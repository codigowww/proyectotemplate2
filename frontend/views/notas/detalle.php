<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = $model->titulo;

Yii::$app->view->params['title-seo'] = $model->titulo;
Yii::$app->view->params['description-seo'] = $model->copete;
?>

<div class="col-lg-10">
    <div class="article-head">

        <h3><?= $model->titulo ?></h3>
        <p><?= $model->copete ?></p>
        <?= $this->render('/common/socials'); ?>
    </div><!-- /.article__head-inner -->

    <div class="article-cuerpo">
        <?= $model->cuerpo ?>
        <div class="fb-comments" data-href="<?= Url::current([], true) ?>" data-width="100%" data-numposts="5"></div>
    </div>
</div>
<div class="col-lg-2">

    <h4 class="others">Otras notas que te pueden interesar</h4>
    <?php if (count($relacionados->models) > 0): ?>
        <?= $this->render('_list_notas', ['dataProvider' => $relacionados]); ?>

    <?php endif; ?>

</div>