var gulp = require('gulp'),
        sass = require('gulp-sass'),
        sourcemaps = require('gulp-sourcemaps'),
        cleanCss = require('gulp-clean-css'),
        rename = require('gulp-rename'),
        postcss = require('gulp-postcss'),
        autoprefixer = require('autoprefixer'),
        imagemin = require('gulp-imagemin'), //Compresor de imágenes
        imageminPngcrush = require('imagemin-pngcrush'), //Optimiza las imágenes .PNG
        watch = require('gulp-watch'), //Sirve para mantener vigilando los cambios de una tarea
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        pump = require('pump'),
        notify = require('gulp-notify'); //Muestra un mensaje callback


gulp.task('compilar-sass', function () {
    return gulp.src(['scss/*.scss'])
            .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(postcss([autoprefixer({browsers: [
                        'Chrome >= 35',
                        'Firefox >= 38',
                        'Edge >= 12',
                        'Explorer >= 10',
                        'iOS >= 8',
                        'Safari >= 8',
                        'Android 2.3',
                        'Android >= 4',
                        'Opera >= 12']})]))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('css/'))
            .pipe(cleanCss())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('css/'))
});



gulp.task('comprimir-imagenes', function () {

        //gulp.src('./img/*.{png,jpg,jpeg,gif,svg}') //Ruta donde buscara las imágenes con extensiones .{png,jpg,jpeg,gif,svg} a comprimir
        
        gulp.src(['img/**/*.{png,jpg,jpeg,gif,svg}'])
        .pipe(
                imagemin({
                    plugins: [imageminPngcrush()] //Optimiza la conversión de imágenes PNG
                }))
        .pipe(gulp.dest('img/min')) //Ruta donde se guardaran la imágenes comprimidas

});



gulp.task('min-js-all', function () {
    gulp.src(['js/*.js', '!js/*.min.js'])
            .pipe(concat('alljs.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('js/'))
});



gulp.task('min-js', function (cb) {
    pump([
        gulp.src(['js/*.js', '!js/*.min.js']),
        uglify(),
        rename({suffix: '.min'}),
        gulp.dest('js/')
    ], cb );
});



gulp.task('auto-sass', ['compilar-sass'], function () {
    console.log("AutoCompilador de SASS , Compila al detectar cambios en archivos scss. Ctrl+C para salir");
    gulp.watch(['scss/*.scss'], ['compilar-sass']);
});

gulp.task('default', function () {
    
    notify("gulp [compilar-sass,auto-sass,min-js,min-js-all,comprimir-imagenes]");
        
        
});



