<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Crear ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-crear">
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= "<?= " ?>Html::encode($this->title) ?></h3></div>

        <div class="panel-body">
            <?= "<?= " ?>$this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>