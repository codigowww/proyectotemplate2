<?php

namespace common\utils;

use \DateTime;

/**
 * Description of Imagenes
 *
 * @author PC03-MAXI
 */
class Utils {

    public static function time_elapsed_string_timestamp($timestamp, $full = false) {
        $ago = new DateTime();
        $ago->setTimestamp($timestamp);
        return self::time_elapsed($ago,$full);
    }

    //Crea una cadena con el tiempo transcurrido desde $datetime ej: hace 2 minutos.
    public static function time_elapsed_string($datetime, $full = false) {
        $ago = new DateTime($datetime);
        return self::time_elapsed($ago,$full);
    }

    private static function time_elapsed($ago, $full = false) {
        $now = new DateTime;

        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;


        //\Yii::error(print_r($diff, true));



        $string = array(
            'y' => 'año',
            'm' => 'mes',
            'w' => 'semana',
            'd' => 'dia',
            'h' => 'hora',
            'i' => 'minuto',
            's' => 'segundo',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? 'Hace ' . implode(', ', $string) : 'Ahora';
    }

    //Limitar $text a $limit cantidad de palabras , agrega "..." al final si supera $limit
    public static function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
