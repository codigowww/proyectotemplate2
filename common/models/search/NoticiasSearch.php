<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Noticias;

/**
 * NoticiasSearch represents the model behind the search form about `common\models\Noticias`.
 */
class NoticiasSearch extends Noticias {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'destacado', 'activo', 'vistas', 'thumb'], 'integer'],
                [['titulo', 'copete', 'cuerpo', 'keywords', 'fecha_creacion', 'fecha_modificacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Noticias::find()->orderBy(['fecha_creacion' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        //Filtro por rango de fecha
        if (!empty($this->fecha_creacion)) {

            Yii::error("filter fecha");
            $fechas_explode = explode(" - ", $this->fecha_creacion);
            if (count($fechas_explode) == 2) {
                $fecha_1 = new \DateTime($fechas_explode[0]);
                $fecha_1->setTime(0, 0);
                $fecha_2 = new \DateTime($fechas_explode[1]);
                $fecha_2->setTime(23, 59);
                $query->andFilterWhere(['between', 'fecha_creacion', $fecha_1->getTimestamp(), $fecha_2->getTimestamp()]);
            }
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'fecha_creacion' => $this->fecha_creacion,
            //'fecha_modificacion' => $this->fecha_modificacion,
            'destacado' => $this->destacado,
            'activo' => $this->activo,
            'vistas' => $this->vistas,
            'thumb' => $this->thumb,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
                ->andFilterWhere(['like', 'copete', $this->copete])
                ->andFilterWhere(['like', 'cuerpo', $this->cuerpo])
                ->andFilterWhere(['like', 'keywords', $this->keywords]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchFront($params) {
        $query = Noticias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_modificacion' => $this->fecha_modificacion,
            'destacado' => $this->destacado,
            'activo' => $this->activo,
            'vistas' => $this->vistas,
            'thumb' => $this->thumb,
        ]);

        $query->andFilterWhere(['like', 'titulo', $this->titulo])
                ->andFilterWhere(['like', 'copete', $this->copete])
                ->andFilterWhere(['like', 'cuerpo', $this->cuerpo])
                ->andFilterWhere(['like', 'keywords', $this->keywords]);

        return $dataProvider;
    }

    public function searchRelacionadas($model, $exclude = null) {


        $query = Noticias::find()->where(['noticias.activo' => 1]);

        if (!is_null($exclude))
            $query->andFilterWhere(['<>', 'noticias.id', $exclude]);

        //$query->andFilterWhere(['like', 'notas_categorias_id', $model->notas_categorias_id]);
        $query->andFilterWhere(['or',
                ['like', 'titulo', $model->keywords],
                ['like', 'copete', $model->keywords],
                ['like', 'cuerpo', $model->keywords],
                ['like', 'keywords', $model->keywords],
                //['notas_categorias_id' => $model->notas_categorias_id],
        ]);

        $query->limit(4);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => \Yii::$app->devicedetect->isMobile() ? 2 : 4,
            ],
        ]);

        return $dataProvider;
    }

}
