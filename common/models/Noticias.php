<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $copete
 * @property string $cuerpo
 * @property integer $fecha_creacion
 * @property integer $fecha_modificacion
 * @property string $keywords
 * @property integer $destacado
 * @property integer $activo
 * @property integer $vistas
 * @property integer $thumb
 *
 * @property Archivos $thumb0
 * @property NoticiasMultimedia[] $noticiasMultimedia
 * @property Archivos[] $archivos
 */ 


class Noticias extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'noticias';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['titulo', 'copete', 'cuerpo'], 'required'],
                [['cuerpo'], 'string'],
                [['fecha_creacion', 'fecha_modificacion', 'destacado', 'activo', 'vistas', 'thumb'], 'integer'],
                [['titulo', 'copete', 'keywords'], 'string', 'max' => 255],
                [['thumb'], 'exist', 'skipOnError' => true, 'targetClass' => Archivos::className(), 'targetAttribute' => ['thumb' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'copete' => 'Copete',
            'cuerpo' => 'Cuerpo',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_modificacion' => 'Fecha Modificacion',
            'keywords' => 'Keywords',
            'destacado' => 'Destacado',
            'activo' => 'Activo',
            'vistas' => 'Vistas',
            'thumb' => 'Thumb',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumb0() {
        return $this->hasOne(Archivos::className(), ['id' => 'thumb']);
    }

    /* @return \yii\db\ActiveQuery
     */

    public function getNoticiasMultimedia() {
        return $this->hasMany(NoticiasMultimedia::className(), ['noticia_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getArchivosMultimedia() {
        return $this->hasMany(Archivos::className(), ['id' => 'archivos_id'])->viaTable('noticias_multimedia', ['noticia_id' => 'id']);
    }

}
