<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property integer $activo
 * @property integer $imagen
 * @property string $keywords
 * @property integer $orden
 * @property integer $fecha_creacion
 * @property integer $fecha_modificacion

 *
 * @property Archivos $imagen0
 * @property ProductosMultimedia[] $productosMultimedia
 * @property Archivos[] $archivos
 * @property ProductosXCategorias[] $productosXCategorias
 * @property ProductosCategorias[] $categorias
 */
class Productos extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'productos';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['nombre', 'activo','orden'], 'required'],
                [['descripcion'], 'string'],
                [['activo', 'imagen', 'orden', 'fecha_creacion', 'fecha_modificacion'], 'integer'],
                [['nombre'], 'string', 'max' => 96],
                [['keywords'], 'string', 'max' => 255],
                [['imagen'], 'exist', 'skipOnError' => true, 'targetClass' => Archivos::className(), 'targetAttribute' => ['imagen' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
            'imagen' => 'Imagen',
            'keywords' => 'Keywords',
            'palabras_clave' => 'Palabras Clave',
            'orden' => 'Orden',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_modificacion' => 'Fecha Modificacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagen0() {
        return $this->hasOne(Archivos::className(), ['id' => 'imagen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductosXCategorias() {
        return $this->hasMany(ProductosXCategorias::className(), ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias() {
        return $this->hasMany(ProductosCategorias::className(), ['id' => 'categoria_id'])->viaTable('productos_x_categorias', ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getProductosMultimedia() {
        return $this->hasMany(ProductosMultimedia::className(), ['producto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getArchivosMultimedia() {
        return $this->hasMany(Archivos::className(), ['id' => 'archivos_id'])->viaTable('productos_multimedia', ['producto_id' => 'id']);
    }

}
