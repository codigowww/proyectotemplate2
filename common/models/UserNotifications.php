<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_notifications".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $descripcion
 * @property integer $fecha
 * @property string $type
 * @property integer $visto
 *
 * @property User $user
 */
class UserNotifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'descripcion', 'fecha', 'type', 'visto'], 'required'],
            [['user_id', 'fecha', 'visto'], 'integer'],
            [['title', 'descripcion', 'type','url'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'descripcion' => 'Descripción',
            'fecha' => 'Fecha',
            'type' => 'Type',
            'visto' => 'Visto',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        
        return $this->hasOne(User::className(), ['id' => 'user_id']);
        
    }
    
    
    public static function getNotificaciones(){
        
        return self::find()->where(['user_id' => Yii::$app->user->id , 'visto' => 0]);
        
    }
    
    
    public static function generarNotificacion($user_id,$title,$mensaje,$url){
        
        $noti = new UserNotifications();
        $noti->user_id = $user_id ; 
        $noti->title = $title;
        $noti->visto = 0;
        $noti->fecha = time();
        $noti->descripcion = $mensaje;
        $noti->type = "info";
        $noti->url = $url;
        $noti->save(false);
        
        
        
        return $noti;
    }
    
}
