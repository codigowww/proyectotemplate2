<?php

namespace backend\widgets;

use Yii;
use yii\widgets\InputWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Archivos;

class MultimediaWidget extends InputWidget {

    public $inputname = "";
    public $previewUrl = "";
    public $multiple = false;
    public $dataprovider_multimedia;
    public $unlink_url;

    /**

      /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init() {
        
    }

    /**
     * @inheritdoc
     */
    public function run() {
        parent::run();

        \backend\assets\ImagenesAsset::register($this->view);
        \backend\assets\MultimediaWidgetAsset::register($this->view);
        \backend\assets\MagnificpopupAsset::register($this->view);

        if ($this->multiple) {
            echo $this->renderInputMultiple();
        } else {
            echo $this->renderInput();
        }
    }

    /**
     * Renders the source Input for the Switch plugin. Graceful fallback to a normal HTML checkbox or radio input in
     * case JQuery is not supported by the browser
     *
     * @return string
     */
    protected function renderInput() {


        $model_archivos = new Archivos();
        $dataProvider_galeria = $model_archivos->searchImagenes();
        $model = $this->model;
        $attribute = $this->attribute;

        return $this->render('multimediaWidgetSingle', [
                    'dataprovider_multimedia' => $this->dataprovider_multimedia,
                    'dataProvider_galeria' => $dataProvider_galeria,
                    'model' => $model,
                    'attribute' => $attribute,
                    'previewUrl' => $this->previewUrl,
                    'wid' => $this->getId(),
        ]);
    }

    protected function renderInputMultiple() {
        $model = new Archivos();
        $dataProvider_galeria = $model->searchImagenes();

        $attribute = $this->attribute;

        return $this->render('multimediaWidget', [
        'dataprovider_multimedia' => $this->dataprovider_multimedia,
        'dataProvider_galeria' => $dataProvider_galeria,
        'unlink_url' => $this->unlink_url,
        'attribute' => $attribute,
        'wid' =>$this->getId(),
        ]);
    }
    
    
    public function renderModals($wid,$enablevideos = false){
        $model = new Archivos();
        $dataProvider_galeria = $model->searchImagenes();
  
        return $this->render('/multimedia/multimediaModals' , compact('dataProvider_galeria','enablevideos', 'wid') );
    }

    protected function getPreviewUrl() {
        
        if (!empty($this->previewUrl))
            return $this->previewUrl;
        else
            return Yii::$app->urlManagerFront->createUrl(['/uploads/nothumb.png']);
    }

    /* protected function initJavascript() {

      }
     */

    /**
     * @return bool whether this widget is associated with a data model.
     */
    protected function hasModel() {
        return $this->model instanceof Model && $this->attribute !== null;
    }

}
