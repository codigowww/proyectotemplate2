<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

class CropperAsset extends AssetBundle
{


    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/cropper/cropper.min.css',
    ];
    public $js = [
        'plugins/cropper/cropper.min.js',
    ];

}
