<?php

namespace backend\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Main backend application asset bundle.
 */
class TablesAsset extends AssetBundle
{


    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'global/vendor/bootstrap-table/bootstrap-table.css'
    ];

    public $js = [

    ];
   public $depends = [
        'backend\assets\TemplateAsset',

    ];
}
