<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserMessages */

$this->title = 'Enviar nuevo mensaje';
$this->params['breadcrumbs'][] = ['label' => 'Mensajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="user-messages-crear">
    <div class="panel panel-primary">

        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>