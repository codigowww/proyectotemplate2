<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use mdm\admin\models\Menu;
use yii\helpers\Json;
use mdm\admin\AutocompleteAsset;


/* @var $this yii\web\View */
/* @var $model mdm\admin\models\Menu */
/* @var $form yii\widgets\ActiveForm */
AutocompleteAsset::register($this);
$opts = Json::htmlEncode([
        'menus' => Menu::getMenuSource(),
        'routes' => Menu::getSavedRoutes(),
    ]);
$this->registerJs("var _opts = $opts;");
$this->registerJs($this->render('_script.js'));



$this->registerCss(" 
.iconpicker-popover{
    width:100%!important;
}
label.icp{

    width: 100%;
}
");

$data = eval($model->data);

$this->registerJs(" 

    $('.icp-auto').iconpicker({ 
        selected: 'fa-".$data['icon']."',
        templates:{
            search: '<input type=\"search\" class=\"form-control iconpicker-search\" placeholder=\"Buscar Ícono\" />',
        }
    });

    $('.icp-auto').on('iconpickerSelected', function(e) {
        $('#icon-input').val(e.iconpickerValue);
    });

");

/*$this->registerCssFile(Url::to('@web/css/fontawesome-iconpicker.min.css'));
$this->registerJsFile(Url::to('@web/js/fontawesome-iconpicker.min.js'),['position' => \yii\web\View::POS_END]);*/


\backend\assets\IconPickerAsset::register($this);

?>

<div class="menu-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= Html::activeHiddenInput($model, 'parent', ['id' => 'parent_id']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 128]) ?>

            <?= $form->field($model, 'parent_name')->textInput(['id' => 'parent_name']) ?>

            
        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'route')->textInput(['id' => 'route']) ?>
            
            <?= $form->field($model, 'order')->input('number') ?>

            <?php //echo $form->field($model, 'data')->textarea(['rows' => 4]) ?>

        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <input type="hidden" name="icono" id="icon-input" >
                <label data-title="Seleccionar Ícono" data-placement="inline" class="icp icp-auto" data-selected="fa-align-justify">
                </label> 
            </div>
        </div>
    </div>

    <div class="form-group">
        <?=
        Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin', 'Update'), ['class' => $model->isNewRecord
                    ? 'btn btn-success' : 'btn btn-primary'])
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
