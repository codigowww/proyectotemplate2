<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Noticias */

$this->title = 'Modificar Noticias: ' . $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titulo, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar';

Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="noticias-modificar">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>


</div>