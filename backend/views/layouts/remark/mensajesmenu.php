<?php

use common\models\UserMessages;
use yii\helpers\Url;
use yii\helpers\Html;

if (!Yii::$app->params['showMessages'])
    return '';

$mensajes = UserMessages::getMensajes();
$mensajes_count = $mensajes->count();
$mensajes_array = $mensajes->all();

$mes_ids = array();
foreach ($mensajes_array as $mes) {
    $mes_ids[] = \yii\helpers\Json::encode($mes->id);
}

/* $this->registerJs('
  var mensajes_opened = false;
  $("#mensajes_dropdown").click(function (){
  if(!mensajes_opened){
  var mensajes = ['.implode(",",$mes_ids).'];
  $.ajax({
  type: "POST",
  dataType: "json",
  url: "'.Url::toRoute(['/mensajes/visto']).'",
  data: { "mensajes" : mensajes }
  }).done(function() {
  $( ".mensajes_count" ).html( "0" );
  });
  }
  mensajes_opened = true;
  });

  '); */
?>


<li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Mensajes"
       aria-expanded="false" data-animation="scale-up" role="button">
        <i class="icon md-email" aria-hidden="true"></i>
        <span class="badge badge-pill badge-info up"><?= $mensajes_count ?></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
        <div class="dropdown-menu-header" role="presentation">
            <h5>MENSAJES</h5>
            <span class="badge badge-round badge-info"><?= $mensajes_count ?> Nuevos</span>
        </div>
        <div class="list-group" role="presentation">
            <div data-role="container">
                <div data-role="content">

                    <?php foreach ($mensajes_array as $mensaje): ?>

                        <a class="list-group-item" href="<?= Url::toRoute(['/user-messages/view' , 'id' => $mensaje->id]); ?>" role="menuitem">
                            <div class="media">
                                <div class="pr-10">
                                    <span class="avatar avatar-sm avatar-online">
                                        <?= Html::img($mensaje->userFrom->profile->getAvatarUrl(), [
                                            //'class' => 'img-circle img-responsive',
                                            'alt' => $mensaje->userFrom->username,
                                        ]) ?>
                                        <i></i>
                                    </span>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading"><?= $mensaje->userFrom->profile->name ?></h6>
                                    <div class="media-meta">
                                        <time datetime="<?= date('d-m-Y h:m:S',$mensaje->fecha) ?>"><?= \common\utils\Utils::time_elapsed_string_timestamp($mensaje->fecha) ; ?> </time>
                                    </div>.
                                    <div class="media-detail"><?= \common\utils\Utils::limit_text($mensaje->message , 15) ?></div>
                                </div>
                            </div>
                        </a>

                    <?php endforeach; ?>

                    
                </div>
            </div>
        </div>
        <div class="dropdown-menu-footer" role="presentation">
            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                <i class="icon md-settings" aria-hidden="true"></i>
            </a>

            <a class="dropdown-item" href="<?= Url::toRoute(['/user-messages/index'])?>" role="menuitem">
                Ver todos los mensajes
            </a>
            
            <a class="dropdown-item" href="<?= Url::toRoute(['/user-messages/create'])?>" role="menuitem">
                Enviar nuevo mensaje
            </a>
        </div>
    </div>
</li>