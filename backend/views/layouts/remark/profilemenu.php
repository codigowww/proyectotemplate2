<?php

use yii\helpers\Url;
use yii\helpers\Html;


$profile = Yii::$app->user->identity->profile;
?>

<li class="nav-item dropdown">
    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
        <span class="avatar avatar-online">
            <?=

            !is_null($profile) ? 
            Html::img($profile->getAvatarUrl(), [
                //'class' => 'img-circle img-responsive',
                'alt' => $profile->user->username,
            ]) : ''
            ?>

            <i></i>
        </span>
    </a>
    <div class="dropdown-menu" role="menu">
        <a class="dropdown-item" href="<?= Url::toRoute(['/user/profile']); ?>" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Perfil</a>
        <a class="dropdown-item" href="<?= Url::toRoute(['/user/settings']); ?>" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Configuración</a>
        <div class="dropdown-divider" role="presentation"></div>
        <!--<a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>-->

        <a class="dropdown-item" href="<?= Url::toRoute(['/site/logout']); ?>" data-method="post" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Salir </a>

    </div>
</li>