<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\widgets\FileInput;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><strong>Subir</strong> </h4>
</div>
<div class="modal-body">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <?php
    echo '<label class="control-label">Imagen</label>';
    echo FileInput::widget([
        'name' => 'imagen-upload',
        'pluginOptions' => [
            //'initialPreview' => $fondo_initialpreview,
            // 'initialPreviewAsData' => true,
            'overwriteInitial' => true,
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => true,
            'uploadUrl' => Url::to(['/imagenes/subir']),
            'minImageWidth' => 1280,
            'minImageHeight' => 720,
        ],
        'pluginEvents' => [
            "fileuploaded" => "function(e, data, previewId, index) { fileinput_uploaded_event(e, data, previewId, index);}",
        ],
    ]); ?>

    <?php ActiveForm::end(); ?>


</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
</div>


<?php
$this->registerJs(<<<JAVASCRIPT
            
        
        
    function fileinput_uploaded_event(e, data, previewId, index){

        //console.log(data);
        $("#modal-subir").trigger( "img-uploaded-event",data);
        $("#modal-subir").modal('toggle');
        toastr["success"]("La imagen ha sido cargada exitosamente!")
        $.pjax.reload({container: '#pjax-imagenes-container', timeout: 10000});
       
    }
   
   
JAVASCRIPT
);
