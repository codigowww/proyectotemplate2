<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
?>


<div id="mediaContent" class="page-content page-content-table <?= empty(Yii::$app->request->get('class')) ? '' : Yii::$app->request->get('class'); ?>" data-plugin="selectable">


    <div class="media-list is-grid pb-50" data-plugin="animateList" data-animate="fade" data-child="li">
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'galeria-input-list',
            'layout' => '<div class="items">{items}</div>{pager}',
            //'layout' => '{items}<div class="pager">{pager}</div>',
            'itemOptions' => ['tag' => 'li', 'class' => 'galeria-input-item'],
            'options' => [
                'tag' => 'ul',
                'class' => '',
            /* 'data-plugin' => "animateList",
              'data-child' => ">li", */
            ],
            'itemView' => '_galeriaItem',
            'pager' => [
                'class' => InfiniteScrollPager::className(),
                'widgetId' => 'galeria-input-list',
                'itemsCssClass' => 'items',
                'nextPageLabel' => 'Cargar más Imágenes',
                'contentLoadedCallback' => 'function(data) { if(data.length < ' . $dataProvider->pagination->pageSize . ') $(".list-notas-actions").hide(); }',
                'linkOptions' => [
                    'class' => 'btn btn-primary btn-loadmore',
                ],
                'pluginOptions' => [
                    'loading' => [
                        'msgText' => "<em>Cargando...</em>",
                        'finishedMsg' => "<em>No se han encontrado mas resultados</em>",
                    ],
                    'behavior' => InfiniteScrollPager::BEHAVIOR_TWITTER,
                ],
            ],
        ]);
        ?>

    </div>

</div>



<?php
$this->registerJs(
        <<<JAVASCRIPT

    $("#modal-multimedia-galeria-$wid").on("click", ".galeria-item-select", function (event) {
        var data = $(this).data();
        $('#modal-multimedia-galeria-$wid').modal('hide');
        select_multimedia_$wid(data.galeriaitemUrl , data.galeriaitemId , data.galeriaitemTipo );
   });    
        
JAVASCRIPT
);
