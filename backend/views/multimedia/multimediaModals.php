<?php

use yii\helpers\Url;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>



<div class="modal fade modal-scroll modal-multimedia-select" id="modal-multimedia-select-<?= $wid ?>" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="btn-group btn-group-lg" aria-label="Large button group" role="group">
                        <button type="button" data-dismiss="modal" data-toggle="modal" data-target="#modal-multimedia-galeria-<?= $wid ?>" class="btn btn-info waves-effect waves-classic"><i class="fa fa-picture-o" aria-hidden="true"></i>Elegir en galería</button>

                        <button type="button" data-dismiss="modal" data-toggle="modal" data-target="#modal-multimedia-add-imagen-<?= $wid ?>" class="btn btn-info waves-effect waves-classic "><i class="fa fa-cloud-upload" aria-hidden="true"></i>Subir nueva imagen</button>
                   
                        <?php if($enablevideos): ?>
                        <button type="button" data-dismiss="modal" data-toggle="modal" data-target="#modal-multimedia-add-video-<?= $wid ?>" class="btn btn-info waves-effect waves-classic "><i class="fa fa-youtube-play" aria-hidden="true"></i>Cargar Video</button>
                        <?php endif; ?>
                        

                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="modal fade modal-multimedia-galeria" id="modal-multimedia-galeria-<?= $wid ?>"  tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Galería</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <?= $this->render('multimediaGaleria', ['dataProvider' => $dataProvider_galeria ,'wid' => $wid]) ?>
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-scroll" id="modal-multimedia-add-imagen-<?= $wid ?>" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <?= $this->render('multimediaUpload',compact('wid')) ?>
        </div>
    </div>
</div>


<div class="modal fade modal-scroll" id="modal-multimedia-recortes" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<div class="modal fade modal-scroll" id="modal-multimedia-add-video-<?= $wid ?>" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
            <?= $this->render('multimediaVideos',compact('wid')) ?>
        </div>
    </div>
</div>
