<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Productos */

$this->title = 'Modificar Productos: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar';
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="productos-modificar">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>


</div>