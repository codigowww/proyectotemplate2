<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\bootstrap\Nav;

$user_logged = Yii::$app->user;
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;
?>

<div class="col-lg-3">
    <!-- Page Widget -->
    <div class="card card-shadow text-center">
        <div class="card-block">
            <a class="avatar avatar-lg" href="javascript:void(0)">
                <?=
                Html::img($profile->getAvatarUrl(230), [
                    'class' => 'img-responsive',
                    'alt' => $profile->user->username,
                ])
                ?>
            </a>
            <h4 class="profile-user"><?= $profile->name ?></h4>
            <p class="profile-job"><?= $profile->user->username ?></p>
            <p><?= $profile->bio ?></p>
            <!--<div class="profile-social">
                <a class="icon bd-twitter" href="javascript:void(0)"></a>
                <a class="icon bd-facebook" href="javascript:void(0)"></a>
                <a class="icon bd-dribbble" href="javascript:void(0)"></a>
                <a class="icon bd-github" href="javascript:void(0)"></a>
            </div>-->
            <p><a href="<?= Url::toRoute(['/user-messages/create', 'to' => $profile->user_id]) ?>" class="btn btn-primary">Enviar Mensaje</a></p>





            <?=
            \common\components\MenuRemark::widget([
                'options' => [
                    'class' => 'nav nav-pills flex-column',
                ],
                'linkTemplate' => '<a href="{url}" class="nav-link {active}">{label}</a>',
                'itemOptions' => ['class' => 'nav-item'],
                'items' => [
                    [
                        'label' => Yii::t('user', 'Account details'),
                        'url' => ['/user/admin/update', 'id' => $user->id]
                    ],
                    [
                        'label' => Yii::t('user', 'Profile details'),
                        'url' => ['/user/admin/update-profile', 'id' => $user->id]
                    ],
                    [
                        'label' => Yii::t('user', 'Information'),
                        'url' => ['/user/admin/info', 'id' => $user->id]
                    
                    ],
                    [
                        'label' => Yii::t('user', 'Assignments'),
                        'url' => ['/permisos/assignment/view-simple', 'id' => $user->id],
                    ],

                    [
                        'label' => Yii::t('user', 'Confirm'),
                        'url' => ['/user/admin/confirm', 'id' => $user->id],
                        'visible' => !$user->isConfirmed,
                        'linkOptions' => [
                            'class' => 'text-success',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Block'),
                        'url' => ['/user/admin/block', 'id' => $user->id],
                        'visible' => !$user->isBlocked,
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Unblock'),
                        'url' => ['/user/admin/block', 'id' => $user->id],
                        'visible' => $user->isBlocked,
                        'linkOptions' => [
                            'class' => 'text-success',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Delete'),
                        'url' => ['/user/admin/delete', 'id' => $user->id],
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to delete this user?'),
                        ],
                    ],

                ],
            ])
            ?>

        </div>
        <!--<div class="card-footer">
            <div class="row no-space">
                <div class="col-4">
                    <strong class="profile-stat-count">260</strong>
                    <span>Follower</span>
                </div>
                <div class="col-4">
                    <strong class="profile-stat-count">180</strong>
                    <span>Following</span>
                </div>
                <div class="col-4">
                    <strong class="profile-stat-count">2000</strong>
                    <span>Tweets</span>
                </div>
            </div>
        </div>-->
    </div>
    <!-- End Page Widget -->
</div>
