<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */
$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>



<div class="page-brand-info">
    <div class="brand">
        <!--<img class="brand-img" src="<?= Url::toRoute('template/assets/images/logo@2x.png'); ?>" alt="...">-->
        <h2 class="brand-text font-size-40">Manifesto</h2>
    </div>
    <p class="font-size-20">Bienvenido al gestor de contenidos web. Por favor ingrese al sitio con su usuario y contraseña en la seccion a su derecha.</p>
</div>
<div class="page-login-main">
    <div class="brand hidden-md-up">
        <!--<img class="brand-img" src="<?= Url::toRoute('template/assets/images/logo-blue@2x.png'); ?>" alt="...">-->
        <h3 class="brand-text font-size-40">Manifesto</h3>
    </div>
    <h3 class="font-size-24">Ingresar</h3>
    <p>Por favor ingrese su usuario y contraseña.</p>

    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => [
                    'class' => 'login-form',
                ],
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ])
    ?>



    <?=
    $form->field($model, 'login', ['options' => ['class' => 'form-group form-material floating' , 'data-plugin' => 'formMaterial' ] /*'labelOptions' => ['class' => 'floating-label']*/,'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
    );
    ?>

    <?=
        $form->field($model, 'password', ['options' => ['class' => 'form-group form-material floating'], 'inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()
        ->label(Yii::t('user', 'Password') . ($module->enablePasswordRecovery ?
                        '(' . Html::a(
                                Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']
                        )
                        . ' )' : '')
        )
    ?>

    <?=
    $form->field($model, 'rememberMe')->checkbox(['tabindex' => '3',])
    ?>

    <?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-primary btn-block', 'tabindex' => '4']) ?>

    <?php ActiveForm::end(); ?>

    <?php if ($module->enableConfirmation): ?>
        <p class="text-center">
            <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
        </p>
    <?php endif ?>
    <?php if ($module->enableRegistration): ?>
        <p class="text-center">
            <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
        </p>
    <?php endif ?>


    <footer class="page-copyright">
        <p>CMS BY Manifesto</p>
        <p>© <?= date('Y',time()) ?>.</p>
        <?=
        Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ])
        ?>

    </footer>
</div>

