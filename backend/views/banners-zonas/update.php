<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BannersZonas */

$this->title = 'Modificar Banners Zonas: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Banners Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar';
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="banners-zonas-modificar">

            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>


</div>

<div class="banners-zonas--banners">

    <?=
    $this->render('_banners', [
        'model' => $model,
        'banners_dataprovider' => $banners_dataprovider,
    ])
    ?>

</div>