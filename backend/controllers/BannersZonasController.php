<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\BannersZonas;
use common\models\search\BannersZonasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;

/**
 * BannersZonasController implements the CRUD actions for BannersZonas model.
 */
class BannersZonasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple','agregar-banner','quitar-banner'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all BannersZonas models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BannersZonasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BannersZonas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $model = $this->findModel($id);

        return $this->renderPartial('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new BannersZonas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new BannersZonas();

        if ($model->load(Yii::$app->request->post())) {


            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido guardados exitosamente.');
                return $this->redirect(['index']);
            } else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        }


        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionDeleteMultiple() {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        return BannersZonas::deleteAll(['id' => $pk]);
    }

    /**
     * Updates an existing BannersZonas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $query_banners = $model->getBanners();

        $banners_dataprovider = new ActiveDataProvider([
            'query' => $query_banners,
            /*'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'title' => SORT_ASC,
                ]
            ],*/
        ]);
        
        
         
                
                
        if ($model->load(Yii::$app->request->post())) {


            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido modificados exitosamente.');
                return $this->redirect(['index']);
            } else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        }


        return $this->render('update', [
                    'model' => $model,
                    'banners_dataprovider' => $banners_dataprovider,
        ]);
    }

    public function actionAgregarBanner($zona) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModel($zona);
        
        $banner = Yii::$app->request->post('banner_selected');
        
        $banner_model = \common\models\Banners::findOne($banner);


        if (is_null($banner_model)) {
            throw new NotFoundHttpException("El banner solicitado no existe");
        }
        
        $model->link('banners', $banner_model);
        return "ok";
    }

    public function actionQuitarBanner($zona, $banner) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModel($zona);
        $banner_model = \common\models\Banners::findOne($banner);

        if (is_null($banner_model)) {
            throw new NotFoundHttpException("El banner solicitado no existe");
        }
        
        $model->unlink('banners', $banner_model,true);
        
        return "ok";
    }

    /**
     * Deletes an existing BannersZonas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {


        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            return Json::encode([
                        'success' => true,
            ]);
        } else
            return $this->redirect(['index']);
    }

    /**
     * Finds the BannersZonas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannersZonas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = BannersZonas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

}
