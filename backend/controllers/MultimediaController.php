<?php

namespace backend\controllers;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Archivos;
use Yii;
use yii\helpers\Json;

class MultimediaController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            /* 'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
              'delete' => ['POST'],
              'delete-multiple' => ['POST'],
              ],
              ], */

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'recortar', 'subir', 'borrar', 'input', 'delete', 'upload-imagen', 'upload-video', 'video-form', 'imagen-form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionImagenForm($wid) {

        return $this->renderPartial('multimediaUpload', ['wid' => $wid]);
    }

    public function actionVideoForm($wid) {
        return $this->renderPartial('multimediaVideos', ['wid' => $wid]);
    }

    public function actionUploadVideo($field_name_post = 'multimedia-youtube-post') {

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {

            $link_youtube_verde = Yii::$app->request->post($field_name_post);


            $link_youtube_validated = '';
            $id_youtube_validated = '';


            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $link_youtube_verde, $match)) {
                $link_youtube_validated = $link_youtube_verde;
                $id_youtube_validated = $match[1];
            } else {
                return Json::encode(
                                [
                                    'success' => false,
                                    'error_report' => 'La dirección enviada es inválida o no fue reconocida del servicio de youtube.',
                                ]
                );
            }

            $archivo_model = new Archivos();
            $archivo_model->ruta = $link_youtube_validated;
            $archivo_model->url = true;
            $archivo_model->nombre = "Youtube Video " . $id_youtube_validated;
            $archivo_model->extension = $id_youtube_validated;
            $archivo_model->fecha_subida = time();
            $archivo_model->imagen_recortes = 0;
            $archivo_model->tipo = "youtube";

            if ($archivo_model->save()) {
                return Json::encode(
                                [
                                    'success' => true,
                                    'uploadedid' => $archivo_model->getPrimaryKey(),
                                    'youtube_url' => $link_youtube_validated,
                                    'youtube_id' => $id_youtube_validated,
                                    'thumb_url' => "https://img.youtube.com/vi/".$id_youtube_validated."/0.jpg",
                                ]
                );
            }

            Yii::error(print_r($archivo_model->errors, true));
            return Json::encode(
                            [
                                'success' => false,
                                'error_report' => 'Hubo un problema al guardar los datos en la base de datos.',
                            ]
            );
        }



        return Json::encode(
                        [
                            'success' => false,
                            'error_report' => 'Petición inválida',
                        ]
        );
    }

    public function actionUploadImagen($field_name_post = 'multimedia-image-upload', $folder = '/galeria/') {


        $validatesize = false ;
        $recorte = false;

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {


            $f_helper = \common\utils\Imagen::uploadArchivo($field_name_post, $folder, true);

            if ($f_helper->hasError) {

                return Json::encode(
                                [
                                    'success' => false,
                                    'error_report' => $f_helper->errorReport
                                ]
                );
            }


            if ($validatesize) :
                if (!$this->imagen_validate_size($f_helper)) {
                    Yii::error("size error");
                    $f_helper->delete();
                    return Json::encode(
                                    [
                                        'success' => false,
                                        'error_report' => 'La imagen debe tener un tamaño mínimo de 1280x720'
                                    ]
                    );
                }
            endif;



            if ($recorte) $f_helper->generarRecortes();

            return Json::encode([
                        'success' => true,
                        'uploadedid' => $f_helper->db_id,
                        'uploadedurl' => $f_helper->getUrl()
            ]);
        }


        return Json::encode(
                        [
                            'success' => false,
                            'error_report' => 'Petición inválida',
                        ]
        );
    }

    protected function imagen_validate_size($f_helper, $w = 1280, $h = 720) {

        $tamaño = getimagesize($f_helper->getFile());

        if ($tamaño[0] < $w || $tamaño[1] < $h) {

            return false;
        }

        return true;
    }

    public function actionGaleria() {
        $model = new Archivos();
        $dataProvider = $model->searchImagenes();

        return $this->renderAjax('galeria', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBorrar($id) {

        //primero chequear si es video o imagen o archivo; IMPLEMENTAR!!


        $imagen = \common\utils\Imagen::loaddb($id);

        if (!is_null($imagen))
            $imagen->delete();
    }

    public function actionDelete() {

        $multimedia_id = Yii::$app->request->post("multimedia_id");

        if (empty($multimedia_id))
            throw new \yii\web\BadRequestHttpException("Parámetros inválidos");


        $imagen = \common\utils\Imagen::loaddb($multimedia_id);

        if (!is_null($imagen))
            $imagen->delete();
    }

    public function actionRecortar($id) {

        
        $archivo_dbr = Archivos::findOne($id);
        
        if(is_null($archivo_dbr)){
            return 'No existe el archivo solicitado';
        }
        
        if($archivo_dbr->url != false){
            
            return 'No se puede recortar links externos!';
        }
        
        $imagen = \common\utils\Imagen::loaddb($id);
        $recortes = \common\utils\Imagen::getRecortes();
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->isPost) {
                $recorte_selected = Yii::$app->request->post('recorte');
                $cropdata = Yii::$app->request->post('cropdata');

                $imagen->cropmanual($cropdata['width'], $cropdata['height'], $cropdata['x'], $cropdata['y'], $imagen->getFiletoRecorte($recorte_selected));
                return Json::encode([
                            'success' => true,
                ]);
            }
            return $this->renderAjax('multimediaCropper', ['id' => $id, 'model' => $imagen, 'recortes' => $recortes]);
        }
        
        return $this->render('multimediaCropper', ['id' => $id, 'model' => $imagen, 'recortes' => $recortes]);
    }

}
