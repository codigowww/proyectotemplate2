<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\ProductosCategorias;
use common\models\search\ProductosCategoriasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * ProductosCategoriasController implements the CRUD actions for ProductosCategorias model.
 */
class ProductosCategoriasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','delete-multiple'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductosCategorias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductosCategoriasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductosCategorias model.
     * @param integer $id
     * @return mixed
     */
     
    public function actionView($id)
    {   

        $model = $this->findModel($id);

        return $this->renderPartial('view', [
            'model' => $model,
        ]);
    }


    /**
     * Creates a new ProductosCategorias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductosCategorias();

        if ($model->load(Yii::$app->request->post())) {


            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido guardados exitosamente.');
                return $this->redirect(['index']);
            }else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        } 


        return $this->render('create', [
            'model' => $model,
        ]);
        
    }


    public function actionDeleteMultiple(){
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        return ProductosCategorias::deleteAll(['id' => $pk]);
    }

    /**
     * Updates an existing ProductosCategorias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {


            if($model->save()){
                \Yii::$app->getSession()->setFlash('success', 'Los datos han sido modificados exitosamente.');
                return $this->redirect(['index']);
            }else
                \Yii::$app->getSession()->setFlash('danger', Html::errorSummary($model));
        } 

            
        return $this->render('update', [
            'model' => $model,
        ]);
        
    }

    /**
     * Deletes an existing ProductosCategorias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {


        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax ) {
            return Json::encode([
                'success' => true,
            ]);
        }
        else return $this->redirect(['index']);
    }

    /**
     * Finds the ProductosCategorias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductosCategorias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductosCategorias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }
}
